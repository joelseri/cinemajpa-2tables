package fr.laerce.cinema.dao;

import fr.laerce.cinema.model.Play;
import fr.laerce.cinema.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserDao extends CrudRepository<User, Long> {
    public List<User> findAllByOrderByIdAsc();
}