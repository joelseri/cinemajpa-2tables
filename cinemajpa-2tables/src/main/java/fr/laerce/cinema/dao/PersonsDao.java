package fr.laerce.cinema.dao;

import fr.laerce.cinema.model.Person;
import fr.laerce.cinema.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface PersonsDao extends CrudRepository<Person, Long> {
    public Person findByImagePath(String img);
}
