package fr.laerce.cinema.dao;

import fr.laerce.cinema.model.Person;
import fr.laerce.cinema.model.Play;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import javax.management.relation.Role;
import java.util.List;

public interface RoleDao extends CrudRepository<Play, Long> {
    public List<Play> findAllByOrderByIdAsc();
}