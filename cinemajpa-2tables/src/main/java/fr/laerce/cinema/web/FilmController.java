package fr.laerce.cinema.web;

import fr.laerce.cinema.dao.*;
import fr.laerce.cinema.dao.PersonsDao;

import fr.laerce.cinema.model.Film;
import fr.laerce.cinema.model.Play;
import fr.laerce.cinema.service.ImageManager;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@Controller
@RequestMapping("/film")
public class FilmController {
    @Autowired
    FilmDao filmDao;
    @Autowired
    PersonsDao personsDao;
    @Autowired
    GenreDao genreDao;
    @Autowired
    RoleDao roleDao;

    @Autowired
    ImageManager imm;

    @GetMapping("/list")
    public String list(Model model){
        Iterable<Film> films = filmDao.findAll();
       /* Iterable<Person> persons = personneDao.findAll();*/
        model.addAttribute("films", films);
       /* model.addAttribute("persons", persons);*/
        return "film/list";
    }

    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id")long id, Model model){
        model.addAttribute("film", filmDao.findById(id).get());
//        model.addAttribute("role", roleDao.findById(id).get());
//        model.addAttribute("role", roleDao.findById(id).get());
//        model.addAttribute("newrole",new Play());
        return "film/detail";
    }

    @GetMapping("/mod/{id}")
    public String mod(@PathVariable("id")long id, Model model){
        model.addAttribute("film", filmDao.findById(id).get());
        model.addAttribute("personnes",personsDao.findAll());
        model.addAttribute("genres",genreDao.findAll());
        model.addAttribute("roles",roleDao.findAll());
        return "film/form";
    }

    @GetMapping("/add")
    public String add(Model model){
        model.addAttribute("film", new Film());
        model.addAttribute("personnes",personsDao.findAll());
        model.addAttribute("genres",genreDao.findAll());
            model.addAttribute("roles",roleDao.findAll());
       /* model.addAttribute("person", new Person());*/
        return "film/form";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id) {
        filmDao.deleteById(id);
      /*  personneDao.deleteById(id);*/
        //on return la chaine string index de façon à ouvrir index.html
        return "film/delete";
    }

    @PostMapping("/add")
    public String submit(@RequestParam("poster")MultipartFile file,  @ModelAttribute Film film){
        if(file.getContentType().equalsIgnoreCase("image/jpeg")){
            try {
                imm.savePoster(film, file.getInputStream());
            } catch (IOException ioe){
                System.out.println("Erreur lecture : "+ioe.getMessage());
            }
        }
        filmDao.save(film);

        return "redirect:/film/list";
    }
    @Value( "${url}" )
    private String url;
    //deuxieme methode pour affichezr  image
    @GetMapping("/affiche/{id}")
    public ResponseEntity<byte[]> getImageAsResponseEntity (HttpServletRequest request, HttpServletResponse response, @PathVariable("id") String id) {
        try {
            HttpHeaders headers = new HttpHeaders ();
            String filename=url+id;
            File i = new File (filename);
            FileInputStream in = new FileInputStream(i);
            byte[] media = IOUtils.toByteArray (in);
            headers.setCacheControl (CacheControl.noCache ().getHeaderValue ());

            ResponseEntity<byte[]> responseEntity = new ResponseEntity<> (media, headers, HttpStatus.OK);
            return responseEntity;
        } catch (IOException e) {
            e.printStackTrace ();
        }
        return null;
    }

}
