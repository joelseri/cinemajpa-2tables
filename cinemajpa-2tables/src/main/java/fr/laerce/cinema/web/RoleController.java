package fr.laerce.cinema.web;


import fr.laerce.cinema.dao.FilmDao;
import fr.laerce.cinema.dao.GenreDao;
import fr.laerce.cinema.dao.PersonsDao;
import fr.laerce.cinema.dao.RoleDao;
import fr.laerce.cinema.model.Genre;
import fr.laerce.cinema.model.Play;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/role")
public class RoleController {

    @Autowired
    RoleDao roleDao;
    @Autowired
    FilmDao filmDao;
    @Autowired
    PersonsDao personsDao;


    @GetMapping("/list")
    public String list(Model model){
        model.addAttribute("roles", roleDao.findAllByOrderByIdAsc());
        return "role/list";
    }

    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") long id, Model model){
        model.addAttribute("role", roleDao.findById(id).get());

        return "role/detail";
    }

    @GetMapping("/mod/{id}")
    public String mod(@PathVariable("id")long id, Model model){
        model.addAttribute("role", roleDao.findById(id).get());
        model.addAttribute("persons", personsDao.findAll());
        model.addAttribute("films", filmDao.findAll());
        return "role/form";
    }

    @GetMapping("/add")
    public String add(Model model){
        model.addAttribute("role", new Play());
        model.addAttribute("persons", personsDao.findAll());
        model.addAttribute("films", filmDao.findAll());

        return "role/form";
    }

    @GetMapping("/delete/{id}")
    public String delete(@  PathVariable("id") Long id){
        roleDao.deleteById(id);
        return "redirect:/role/list";
    }

    @PostMapping("/add")
    public String submit(@ModelAttribute Play play){
      roleDao.save(play);
        return "redirect:/role/list";
    }
}


